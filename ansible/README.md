## Pre-requisites:

On your local linux machine:

* Installed [Ansible 2.8.2](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

On managed linux machines: 

* Installed Ubuntu Server 18.10




## Usage:

```git clone https://github.com/achelak/ansible.git```

```cd ansible```

```change inventory host```

```cd ansible/roles/dbms_mysql_redis/defaults && change default vars```

```cd ansible/roles/nginx_php-fpm_deploy/defaults && change default vars```

```change vars roles 'action' for control execution tasks```

```run ansible playbook```

## After VM's installation and provisioning

```check default home page: curl {{ ansible_default_ipv4['address'] }}```

```chek mysql slave replication status: SHOW SLAVE STATUS \G;```